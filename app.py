from flask import Flask, render_template, jsonify
import requests
from flask import *
import settings
from geopy.geocoders import Nominatim
import googlemaps
import datetime
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
import smtplib
from threading import Timer
from apscheduler.scheduler import Scheduler
import time
import os
import logging
print "starting"

app = Flask(__name__)

search_url = "https://maps.googleapis.com/maps/api/place/textsearch/json"
details_url = "https://maps.googleapis.com/maps/api/place/details/json"

@app.route("/", methods=["GET"])
def retreive():
    return render_template('uber.html') 

@app.route("/sendRequest/<string:query>")
def results(query):
	search_payload = {"key": settings.GOOGLE_API_KEY, "query": query}
	search_req = requests.get(search_url, params=search_payload)
	search_json = search_req.json()

	place_id = search_json["results"][0]["place_id"]

	details_payload = {"key": settings.GOOGLE_API_KEY, "placeid": place_id}
	details_resp = requests.get(details_url, params=details_payload)
	details_json = details_resp.json()

	url = details_json["result"]["url"]
	return jsonify({'result': url})

@app.route('/get_eta', methods=['POST','GET'])
def get_eta():
	gmaps = googlemaps.Client(key=settings.GOOGLE_API_KEY)
	# print "form ", request.form
	hour = request.form['hrs']
	mins = request.form['mins']
	src = request.form['src']
	email = request.form['email']
	now = datetime.datetime.now()
	midnight = now.replace(hour=0, minute=0, second=0, microsecond=0)
	requested_time_df = datetime.datetime(now.year, now.month, now.day, int(hour), int(mins))
	# print src
	src_location = gmaps.geocode(src)[0].get('geometry', {}).get('location', {})
	src_lat = src_location.get('lat')
	src_lng = src_location.get('lng')
	dest = request.form['dest']
	# print src_lat, src_lng
	dest_location = gmaps.geocode(dest)[0].get('geometry', {}).get('location', {})
	dest_lat = dest_location.get('lat')
	dest_lng = dest_location.get('lng')
	# print dest_lat, dest_lng
	data = request_uber(src_lat, src_lng, dest_lat, dest_lng)
	times = data.get("times")
	eta = 0
	for prod in times:
		if(prod.get("display_name") == "uberGO"):
			print prod.get("estimate")
			eta = int(prod.get("estimate"))
	# adding 15 minutes which is handled by 5 minute requests before mail in the send_email function
	pessimistic_eta = 900 + eta 
	req_secs_from_midnight = (requested_time_df - midnight).total_seconds()
	dt_jan_1_1970 = datetime.datetime(1970, 1, 1)
	req_secs_from_jan_1_1970 = (requested_time_df - dt_jan_1_1970).total_seconds()
	# print "req_secs_from_midnight : ", req_secs_from_midnight
	# print "req_secs_from_jan_1_1970 :", req_secs_from_jan_1_1970
	google_estimate = request_google(src_lat, src_lng, dest_lat, dest_lng, req_secs_from_jan_1_1970)
	# print "google_estimate ", google_estimate
	google_prediction_seconds = int(google_estimate['rows'][0]['elements'][0]['duration']['value'])
	google_prediction_milli = google_prediction_seconds * 1000
	google_prediction_text = google_estimate['rows'][0]['elements'][0]['duration']['text']
	flt_uber_eta = float(int(eta)/60)
	now_seconds = (now - midnight).seconds
	requested_seconds = (requested_time_df - midnight).seconds - pessimistic_eta
	# print "now_seconds ", str(now_seconds)
	# print "requested_seconds ", str(requested_seconds)
	# print "request must be made after " + str(requested_seconds - google_prediction_seconds - now_seconds) + " seconds"
	# print "response: ", data
	email_after_seconds = (requested_seconds - google_prediction_seconds - now_seconds)
	email_after_milli_seconds = (email_after_seconds) * 1000
	# print 'requested_time_df ', requested_time_df
	print 'email_after_seconds ', email_after_seconds
	# print 'timedelta ', (datetime.datetime.now() + datetime.timedelta(seconds=email_after_seconds))
	email_time = (datetime.datetime.now() + datetime.timedelta(seconds=email_after_seconds))
	logs = str(datetime.datetime.now()) + " >> email_time for "+ str(email)+ " is "+ str(email_time)
	print str(datetime.datetime.now()) + " >> email_time for ", email, " is ", email_time

	#sending mail right away if time exceeds
	if (datetime.datetime.now()> email_time):
		send_email(email, email_time, requested_time_df, google_prediction_seconds, src_lat, src_lng, dest_lat, dest_lng, google_prediction_text, flt_uber_eta, logs)
	else:	
		sched = Scheduler()
		sched.add_date_job(send_email, email_time, args=[email, email_time, requested_time_df, google_prediction_seconds, src_lat, src_lng, dest_lat, dest_lng, google_prediction_text, flt_uber_eta, logs])
		logging.basicConfig()
		sched.start()
	return jsonify({'milli_seconds': email_after_milli_seconds})


def test_scheduler():
	print "TEST " + str(datetime.datetime.now())


def request_uber(src_lat, src_lng, dest_lat, dest_lng):
	url = 'https://api.uber.com/v1/estimates/time'
	# prod_id_ubergo = 'c8170d76-b67c-44b1-8c26-5f45541434d2'
	parameters = {
		'server_token': settings.UBER_API_KEY,
		'start_latitude': float(src_lat),
		'start_longitude': float(src_lng),
		'end_latitude': float(dest_lat),
		'end_longitude': float(dest_lng),
		'localized_display_name': 'uberGo',
	}
	headers = {'Authorization': 'Token' + settings.UBER_API_KEY}
	# print parameters
	response = requests.get(url, params=parameters, headers=headers)

	data = response.json()
	return data

def request_google(src_lat,src_lng,dest_lat,dest_lng,req_secs_from_jan_1_1970):
	google_estimate = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins="+str(src_lat)+","+str(src_lng)+"&destinations="+str(dest_lat)+","+str(dest_lng)+"&key="+settings.GOOGLE_API_KEY+"&arrival_time="+str(req_secs_from_jan_1_1970)
	response = requests.get(google_estimate)
	data = response.json()
	return data

@app.route('/send_email/<string:email>', methods=['POST','GET'])
def send_email(email, email_time, requested_time_df, google_prediction_seconds, src_lat, src_lng, dest_lat, dest_lng, google_prediction_text , flt_uber_eta, logs):
	# print "in here..."
	data = request_uber(src_lat, src_lng, dest_lat, dest_lng)
	times = data.get("times")
	eta = 0
	for prod in times:
		if(prod.get("display_name") == "uberGO"):
			print prod.get("estimate")
			eta = int(prod.get("estimate"))
	flt_uber_eta = float(int(eta)/60)
	possible_time = requested_time_df - datetime.timedelta(seconds=google_prediction_seconds) - datetime.timedelta(seconds=eta)
	#adding 1 minute to handle server delay failures
	email_time = email_time + datetime.timedelta(seconds=60)
	print "possible_time ", possible_time
	print "email_time :", email_time
	print "(email_time - datetime.timedelta(seconds=300) :", (email_time + datetime.timedelta(seconds=300))
	print (email_time > possible_time)
	print ((email_time + datetime.timedelta(seconds=300)) > possible_time)
	#sending mail right away if time exceeds or lies in a time difference of 5 minutes or exceeds possible time
	if ((email_time < datetime.datetime.now())  or (email_time > possible_time) or ((email_time + datetime.timedelta(seconds=300)) > possible_time)):
		print "date: ", datetime.datetime.now()
		print "starting mail for " + email
		fromaddr = "samplemailer12@gmail.com"
		toaddr = email
		msg = MIMEMultipart()
		msg['From'] = fromaddr
		msg['To'] = toaddr
		msg['Subject'] = "Time to book uber"
		body = "Requested time :"+str(requested_time_df)+"\nTime to book uber at " + str(datetime.datetime.now()) + " google_eta = "+google_prediction_text+" + uber_eta = " + str(flt_uber_eta)
		body = body + "\n"+logs
		msg.attach(MIMEText(body, 'plain'))
		server = smtplib.SMTP('smtp.gmail.com', 587)
		server.ehlo()
		server.starttls()
		server.ehlo()
		server.login("samplemailer12@gmail.com", "samplemailer")
		text = msg.as_string()
		server.sendmail(fromaddr, toaddr, text)
		msg = str(datetime.datetime.now()) + " Request sent to uber for " + email
		return msg
	else:
		sched = Scheduler()
		email_time = email_time + datetime.timedelta(seconds=300)
		logs = logs + "\n" + str(datetime.datetime.now()) + " >> new email_time for "+str(email)+" is "+str(email_time) 
		print str(datetime.datetime.now()) + " >> new email_time for ",email," is ", email_time
		sched.add_date_job(send_email, email_time, args=[email, email_time, requested_time_df, google_prediction_seconds, src_lat, src_lng, dest_lat, dest_lng, google_prediction_text , flt_uber_eta, logs])
		logging.basicConfig()
		sched.start()

if __name__ == "__main__":
	app.run(debug=True, port=8888)
